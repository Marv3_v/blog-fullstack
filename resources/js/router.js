import Vue from "vue";
import VueRouter from "vue-router";

import PageHome from "./pages/Home";
import PageOtra from "./pages/Otro";

Vue.use(VueRouter);


const router = new VueRouter({
    mode: "history",
    routes: [
        {
            path: "/",
            component: PageHome,
        },
        {
            path: "/otro",
            component: PageOtra, 
        }
    ],

});

export default router;

