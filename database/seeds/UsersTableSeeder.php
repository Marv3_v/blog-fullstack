<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
                    [
                        'name' => 'MarkVillas',
                        'email' => 'mark@gmail.com',
                        'password' => bcrypt('mark1234'),
                    ],
                    [
                        'name' => 'Jep',
                        'email' => 'jep@gmail.com',
                        'password' => bcrypt('jep1234'),
                    ],
                    [
                        'name' => 'Clavero',
                        'email' => 'clavero@gmail.com',
                        'password' => bcrypt('clavero1234'),
                    ],
                ];

        DB::table('users')->insert($users);

    }
}
