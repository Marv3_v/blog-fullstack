<?php

namespace App\Http\Controllers;

use App\Post;
use Spatie\Searchable\Search;
use Illuminate\Http\Request;

class PostSearchController extends Controller
{
    public function index(Request $request)
    {
        $results = (new Search())
            ->registerModel(Post::class, 'message')
            ->search($request->input('query'));

        return response()->json($results);
    }
}