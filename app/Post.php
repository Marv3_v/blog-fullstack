<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;

class Post extends Model implements Searchable
{
    protected $fillable = ['message', 'user_id'];

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function getSearchResult(): SearchResult
    {
        $url = route('posts.show', $this->id);
        return new SearchResult($this, $this->message, $url);
    }
}
